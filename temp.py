import time
import socket
import psutil
import paho.mqtt.client as mqtt
from subprocess import PIPE, Popen

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
def get_cpu_temperature():
    """get cpu temperature using vcgencmd"""
    process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)
    output, _error = process.communicate()
    outputstr = output.decode("utf-8")
    return float(outputstr[outputstr.index('=') + 1:outputstr.rindex("'")])
def memory():
    memory = psutil.virtual_memory()
    # Divide from Bytes -> KB -> MB
    used = round(memory.used/1024.0/1024.0,1)
    total = round(memory.total/1024.0/1024.0,1)
    return str(used) + '/' + str(total) + ' MB'

client = mqtt.Client()

client.on_connect = on_connect
client._ssl_context
client.connect("192.168.22.26", 1883, 60)

client.loop_start()

while True:
    time.sleep(30)
    client.publish("openhab/"+socket.gethostname()+"/temperature", get_cpu_temperature())
    client.publish("openhab/"+socket.gethostname()+"/memory", memory())                                    
